using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VREndButton : MonoBehaviour
{

    public bool vrActivated = false;
    public GameObject leftDoor, rightDoor;

    private void OnTriggerStay(Collider other)
    {
        leftDoor.transform.position = leftDoor.transform.position + new Vector3(-3, 0, 0);
        rightDoor.transform.position = rightDoor.transform.position + new Vector3(3, 0, 0);
    }

}
