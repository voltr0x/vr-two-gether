using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePlatforms : MonoBehaviour
{
    bool activate = false;
    public GameObject plat1, plat2, plat3, plat4;
    Vector3 plat1TargetPosition, plat2TargetPosition, plat3TargetPosition, plat4TargetPosition;

    [SerializeField] private GameObject[] platforms;
    [SerializeField] private Vector3[] platTargetPos;

    [SerializeField] float platformSpeed = 3.5f;

    void Start()
    {
        platTargetPos = new Vector3[4];
        
        for (int i=0; i<4; ++i)
        {
            platTargetPos[i] = new Vector3(platforms[i].transform.position.x, 5, platforms[i].transform.position.z);
        }
    }
    void Update()
    {
        float step = platformSpeed * Time.deltaTime;

        if (activate)
        {
            for (int i = 0; i<4; ++i)
            {
                platforms[i].transform.position = Vector3.MoveTowards(platforms[i].transform.position, platTargetPos[i], step);
                StartCoroutine("WaitForPlatformActivation");
            }
            activate = false;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        // TO-DO: Add a flag for VR player
        activate = true;
    }

    IEnumerator WaitForPlatformActivation()
    {
        yield return new WaitForSeconds(5);
    }

}
