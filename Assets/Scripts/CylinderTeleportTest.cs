using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderTeleportTest : MonoBehaviour
{

    GameObject player;
    public GameObject teleportObject;
    bool isColliding = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (isColliding && Input.GetKeyDown("e"))
        {
            player.transform.position = teleportObject.transform.position;
            Debug.Log("colliding");
        }
        else
        {
            //Debug.Log("not colliding");
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isColliding = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isColliding = false;
        }
    }

}
