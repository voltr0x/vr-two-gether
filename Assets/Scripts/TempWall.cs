using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempWall : MonoBehaviour
{
    private int count = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(count >= 3)
        {
            this.gameObject.SetActive(false);
            count = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);

        if (other.CompareTag("Bullet"))
        {
            count++;
            Debug.Log("Hit!");
        }
    }
}
