using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayHint : MonoBehaviour
{
    public TextMeshProUGUI hintTip;
    bool showTip = true;

    // Start is called before the first frame update
    void Start()
    {
        hintTip.enabled = false; 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerPC") && showTip)
        {
            hintTip.enabled = true;
            Invoke("TurnHintOff", 3);
        }
    }

    void TurnHintOff()
    {
        hintTip.enabled = false;
    }
}
