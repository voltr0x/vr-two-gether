using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldToggle : MonoBehaviour
{

    public GameObject shield;
    bool shieldActive = false;

    // Start is called before the first frame update
    void Start()
    {
        shield.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            shieldActive = true;
        }

        else
        {
            shieldActive = false;
        }

        if (shieldActive)
        {
            shield.SetActive(true);
        }

        else
        {
           shield.SetActive(false);
        }
    }
}
