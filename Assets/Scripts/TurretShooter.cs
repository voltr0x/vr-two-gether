using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShooter : MonoBehaviour
{

    public GameObject turretProjectile;
    public Transform turretTip;
    public float turretFirerate = 5.0f;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        turretFirerate -= Time.deltaTime;

        if (turretFirerate <= 0f)
        {
            GameObject bullet = Instantiate(turretProjectile, turretTip.position + turretTip.forward, turretTip.rotation);
            bullet.GetComponent<Rigidbody>().AddForce(0, 0, -100);
            Destroy(bullet, 10.0f);
            turretFirerate = 5.0f;
        }
        

    }
}
