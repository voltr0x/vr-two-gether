using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTree : MonoBehaviour
{

    public GameObject tree;
    public GameObject infection, infection2;
    bool isRotated = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        // replace with appropriate VR input
        if (Input.GetKeyDown(KeyCode.E) && !isRotated)
        {
            tree.transform.Translate(4, -2, 0, Space.Self);
            
            // Fall slowly
            if (tree.transform.rotation.eulerAngles.z < -80)
            {
                tree.transform.Rotate(new Vector3(0, 0, -30) * Time.deltaTime);
            }
            // tree.transform.Rotate(0, 0, -80);
            isRotated = true;
            Destroy(infection);
            Destroy(infection2);
        }
    }
}
