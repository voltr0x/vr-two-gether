using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrapHoleLevelChange : MonoBehaviour
{
    public GameObject playerPC, playerVR;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("adsfrespawning");
        if (other.gameObject.CompareTag("PlayerPC"))
        {
            SceneManager.LoadScene("Level 2 Training");
        }

        if (other.gameObject.CompareTag("PlayerVR"))
        {
            SceneManager.LoadScene("Level 2 Training");
        }
    }

}
