# VR Two-gether

## https://www.youtube.com/watch?v=CNwsZMGQIb4

VR Two-gether is a cross-platform asymmetric cooperative game in which one player uses a VR headset and controllers and the other player plays on a PC. Both players need to collaborate and solve cooperative puzzles in order to progress through the game. Within this game, we were seeking whether the asymmetric controls in VR and PC interfere or assist with the shared intentionality in a cross-platform cooperative multiplayer game.
